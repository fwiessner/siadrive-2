#include <siaapi.h>
#include <siadriveconfig.h>

#include <utility>

using namespace Sia::Api;

CSiaApi::_CSiaConsensus::_CSiaConsensus(std::shared_ptr<CSiaDriveConfig> siaDriveConfig) :
  CSiaBase(std::move(siaDriveConfig)),
  _Height(0),
  _Synced(false),
  _CurrentBlock("") {
}

CSiaApi::_CSiaConsensus::~_CSiaConsensus() = default;

void CSiaApi::_CSiaConsensus::Refresh() {
  json result;
  if (ApiSuccess(GetSiaDriveConfig()->GetSiaCommunicator()->Get("/consensus", result))) {
    SetHeight(result["height"].get<std::uint64_t>());
    SetSynced(result["synced"].get<bool>());
    SetCurrentBlock(result["currentblock"].get<std::string>());
  } else {
    SetHeight(GetHeight());
    SetSynced(false);
    SetCurrentBlock(GetCurrentBlock());
  }
}
