#include <siacurl.h>
#include <curl/curl.h>
#include <eventsystem.h>
#include <siaapi.h>

using namespace Sia::Api;
BEGIN_EVENT(SiaCurlBegin)
  public:
    SiaCurlBegin(const bool &isPost, const SString &url, const SString &data = "") :
      CEvent(EventLevel::Verbose),
      _type(isPost ? "POST" : "GET"),
      _url(url),
      _data(data) {
    }

  private:
    SiaCurlBegin(const SString &type, const SString &url, const SString &data = "") :
      CEvent(EventLevel::Verbose),
      _type(type),
      _url(url),
      _data(data) {
    }

  private:
    SiaCurlBegin() :
      CEvent(EventLevel::Verbose) {
    }

  public:
    virtual ~SiaCurlBegin() {
    }

  private:
    SString _type;
    SString _url;
    SString _data;

  protected:
    virtual void LoadData(const json& j) override {
      _type = j["type"].get<std::string>();
      _url = j["url"].get<std::string>();
      _data = j["data"].get<std::string>();
    }

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|" + _type + "|URL|" + _url + (((_type == "POST") && _data.Length()) ? "|DATA|" + _data : "");
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new SiaCurlBegin(_type, _url, _data));
    }

    virtual json GetEventJson() const override {
      return {{"event", GetEventName()},
              {"type",  _type},
              {"url",   _url},
              {"data",  _data}};
    }
END_EVENT(SiaCurlBegin);

BEGIN_EVENT(SiaCurlEnd)
  public:
    SiaCurlEnd(const bool &isPost, const SString &url, const CURLcode &curlCode, const SiaCommError &siaCommError) :
      CEvent(EventLevel::Verbose),
      _type(isPost ? "POST" : "GET"),
      _url(url),
      _curlCode(curlCode),
      _reason(siaCommError.GetReason()) {
    }

  private:
    SiaCurlEnd(const SString &type, const SString &url, const CURLcode &curlCode, const SString &reason) :
      CEvent(EventLevel::Verbose),
      _type(type),
      _url(url),
      _curlCode(curlCode),
      _reason(reason) {
    }

  private:
    SiaCurlEnd() :
      CEvent(EventLevel::Verbose) {
    }

  public:
    virtual ~SiaCurlEnd() {
    }

  private:
    SString _type;
    SString _url;
    CURLcode _curlCode;
    SString _reason;

  protected:
    virtual void LoadData(const json& j) override {
      _type = j["type"].get<std::string>();
      _url = j["url"].get<std::string>();
      _curlCode = (CURLcode)j["code"].get<int>();
      _reason = j["reason"].get<std::string>();
    }

  public:
    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|" + _type + "|URL|" + _url + "|CODE|" + SString::FromInt32(_curlCode) + "|ERROR|" + _reason;
    }

    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new SiaCurlEnd(_type, _url, _curlCode, _reason));
    }

    virtual json GetEventJson() const override {
      return {{"event",  GetEventName()},
              {"type",   _type},
              {"url",    _url},
              {"code",   _curlCode},
              {"reason", _reason}};
    }
END_EVENT(SiaCurlEnd);


CSiaCurl::CSiaCurl() {
  SetHostConfig({"localhost",
                 9980,
                 ""});
}

CSiaCurl::~CSiaCurl() = default;

SString CSiaCurl::UrlEncode(const SString &data, const bool &allowSlash) {
  CURL *curlHandle = curl_easy_init();
  curl_easy_reset(curlHandle);
  char *value = curl_easy_escape(curlHandle, data.str().c_str(), 0);
  SString ret = value;
  curl_free(value);
  if (allowSlash) {
    ret.Replace("%2F", "/");
  }
  curl_easy_cleanup(curlHandle);
  return ret;
}

SString CSiaCurl::GetApiErrorMessage(SString result) {
  SString ret;
  result = result.Replace("\r", "").Replace("\n", "").Trim();
  if (result.BeginsWith("{") && result.EndsWith("}")) {
    json jsonResult = json::parse(result.str().c_str());
    if (jsonResult.find("message") != jsonResult.end()) {
      ret = jsonResult["message"].get<std::string>();
    }
  } else if (result.Length() > 0) {
    ret = result.ReplaceCopy("\n", "");
  }
  return ret;
}

std::string CSiaCurl::ConstructPath(const SString &relativePath) const {
  const std::string ret = "http://" + GetHostConfig().HostName + ":" + std::to_string(GetHostConfig().HostPort) + UrlEncode(relativePath, true);
  return ret;
}

SiaCommError CSiaCurl::ProcessResponse(const int &res, const int &httpCode, const std::string &result, json &response) const {
  SiaCommError ret;
  if ((res == CURLE_OK) && ((httpCode >= 200) && (httpCode < 300))) {
    if (result.length()) {
      response = json::parse(result.c_str());
    }
  } else {
    if ((res == CURLE_COULDNT_RESOLVE_HOST) || (res == CURLE_COULDNT_CONNECT)) {
      ret = {SiaCommErrorCode::NoResponse,
             SString::FromInt32(res)};
    } else if (res == CURLE_OPERATION_TIMEDOUT) {
      ret = {SiaCommErrorCode::Timeout,
             SString::FromInt32(httpCode ? httpCode : res)};
    } else if (httpCode) {
      ret = {SiaCommErrorCode::HttpError,
             SString::FromInt32(httpCode) + ":" + (result.length() ? GetApiErrorMessage(result) : "")};
    } else {
      ret = {SiaCommErrorCode::UnknownFailure,
             SString::FromInt32(res)};
    }
  }
  return ret;
}

SiaCommError CSiaCurl::_Get(const SString &path, const HttpParameters &parameters, json &response) const {
  CURL *curlHandle = curl_easy_init();
  curl_easy_reset(curlHandle);
  SString url = ConstructPath(path);
  if (parameters.size()) {
    url += "?";
    for (const auto &param : parameters) {
      if (url[url.Length() - 1] != '?') {
        url += "&";
      }
      url += (param.first + "=" + UrlEncode(param.second));
    }
  }
  curl_easy_setopt(curlHandle, CURLOPT_TIMEOUT_MS, GetHostConfig().TimeoutMs);
  curl_easy_setopt(curlHandle, CURLOPT_USERAGENT, "Sia-Agent");
  curl_easy_setopt(curlHandle, CURLOPT_URL, url.str().c_str());
  curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, static_cast<size_t(*)(char *, size_t, size_t, void *) > ([](char *buffer, size_t size, size_t nitems, void *outstream) -> size_t {
    (*reinterpret_cast<SString *>(outstream)) += std::string(buffer, size * nitems);
    return size * nitems;
  }));
  SString result;
  curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &result);
  CEventSystem::EventSystem.NotifyEvent(std::make_shared<SiaCurlBegin>(false, url));
  const CURLcode res = curl_easy_perform(curlHandle);
  long httpCode = 0;
  curl_easy_getinfo(curlHandle, CURLINFO_RESPONSE_CODE, &httpCode);
  SiaCommError ret = ProcessResponse(res, httpCode, result, response);
  curl_easy_cleanup(curlHandle);
  CEventSystem::EventSystem.NotifyEvent(std::make_shared<SiaCurlEnd>(false, url, res, ret));
  return ret;
}

bool CSiaCurl::CheckVersion(SiaCommError &error) const {
  error = SiaCommErrorCode::InvalidRequiredVersion;
  if (GetHostConfig().RequiredVersion.Length()) {
    error = SiaCommErrorCode::NoResponse;
    const SString serverVersion = GetServerVersion();
    if (serverVersion.Length()) {
      error = (serverVersion == GetHostConfig().RequiredVersion) ? SiaCommErrorCode::Success : SiaCommErrorCode::ServerVersionMismatch;
    }
  }
  return ApiSuccess(error);
}

SString CSiaCurl::GetServerVersion() const {
  json response;
  if (ApiSuccess(_Get("/daemon/version", {}, response))) {
    return response["version"].get<std::string>();
  }
  return "";
}

SiaCommError CSiaCurl::Get(const SString &path, json &response) const {
  SiaCommError ret;
  if (CheckVersion(ret)) {
    ret = _Get(path, {}, response);
  }
  return ret;
}

SiaCommError CSiaCurl::Get(const SString &path, const HttpParameters &parameters, json &response) const {
  SiaCommError ret;
  if (CheckVersion(ret)) {
    ret = _Get(path, parameters, response);
  }
  return ret;
}

SiaCommError CSiaCurl::Post(const SString &path, const HttpParameters &parameters, json &response) const {
  SiaCommError ret;
  if (CheckVersion(ret)) {
    CURL *curlHandle = curl_easy_init();
    curl_easy_reset(curlHandle);
    if (path != "/wallet/unlock") {
      curl_easy_setopt(curlHandle, CURLOPT_TIMEOUT_MS, GetHostConfig().TimeoutMs);
    }
    curl_easy_setopt(curlHandle, CURLOPT_USERAGENT, "Sia-Agent");
    curl_easy_setopt(curlHandle, CURLOPT_URL, ConstructPath(path).c_str());
    curl_easy_setopt(curlHandle, CURLOPT_WRITEFUNCTION, static_cast<size_t(*)(char *, size_t, size_t, void *) > ([](char *buffer, size_t size, size_t nitems, void *outstream) -> size_t {
      (*reinterpret_cast<SString *>(outstream)) += std::string(buffer, size * nitems);
      return size * nitems;
    }));
    SString fields;
    for (const auto &param : parameters) {
      if (fields.Length()) {
        fields += "&";
      }
      fields += (param.first + "=" + param.second);
    }
    std::string utf8Fields = fields;
    curl_easy_setopt(curlHandle, CURLOPT_POSTFIELDS, &utf8Fields[0]);
    SString result;
    curl_easy_setopt(curlHandle, CURLOPT_WRITEDATA, &result);
    CEventSystem::EventSystem.NotifyEvent(std::make_shared<SiaCurlBegin>(true, ConstructPath(path), fields));
    const CURLcode res = curl_easy_perform(curlHandle);
    long httpCode = 0;
    curl_easy_getinfo(curlHandle, CURLINFO_RESPONSE_CODE, &httpCode);
    ret = ProcessResponse(res, httpCode, result, response);
    curl_easy_cleanup(curlHandle);
    CEventSystem::EventSystem.NotifyEvent(std::make_shared<SiaCurlEnd>(false, ConstructPath(path), res, ret));
  }
  return ret;
}
