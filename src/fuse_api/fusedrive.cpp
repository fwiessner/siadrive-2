#include "fusedrive.h"

#define FUSE_USE_VERSION 29
#ifdef __APPLE__
#include <osxfuse/fuse.h>
#include <sys/vnode.h>
#include <sys/param.h>
#include <sys/time.h>
#include <sys/attr.h>
#define G_PREFIX			"org"
#define G_KAUTH_FILESEC_XATTR G_PREFIX 	".apple.system.Security"
#define A_PREFIX			"com"
#define A_KAUTH_FILESEC_XATTR A_PREFIX 	".apple.system.Security"
#define XATTR_APPLE_PREFIX		"com.apple."
#else
#include <fuse/fuse.h>
#include <fuse/fuse_lowlevel.h>
#include <fcntl.h>
#include <sys/file.h>
#endif

#include <siaapi.h>
#include <filepath.h>
#include <siadriveconfig.h>
#include <unistd.h>
#include <dirent.h>
#include <sys/xattr.h>
#include <siavfs.h>

using namespace std::literals::chrono_literals;
using namespace Sia::Api;
using namespace Sia::Api::FUSE;

BEGIN_EVENT(FuseEvent)
  public:
    FuseEvent(const SString& functionName, const int &result, const SString &siaPath, const SString& filePath) :
      CEvent(EventLevel::Debug),
      _functionName(functionName),
      _result(result),
      _siaPath(siaPath),
      _filePath(filePath) {
    }

  private:
    FuseEvent() :
      CEvent(EventLevel::Debug) {
    }

  public:
    virtual ~FuseEvent() {
    }

  private:
    SString _functionName;
    int _result;
    SString _siaPath;
    SString _filePath;
    
  protected:
    virtual void LoadData(const json& j) override {
      _functionName = j["function_name"].get<std::string>();
      _result = j["result"].get<int>();
      _siaPath = j["sia_path"].get<std::string>();
      _filePath = j["file_path"].get<std::string>();
    }

  public:
    virtual std::shared_ptr<CEvent> Clone() const override {
      return std::shared_ptr<CEvent>(new FuseEvent(_functionName, _result, _siaPath, _filePath));
    }

    virtual SString GetSingleLineMessage() const override {
      return GetEventName() + "|FUNC|" + _functionName + "|RES|" + SString::FromUInt32(_result) + "|SP|" + _siaPath + "|FP|" + _filePath;
    }

    virtual json GetEventJson() const override {
      return {{"event",         GetEventName()},
              {"function_name", _functionName},
              {"result",        _result},
              {"file_path",     _filePath},
              {"sia_path",      _siaPath}};
    }
END_EVENT(FuseEvent);

CEventPtr CreateFuseEvent(const SString &functionName, const int &ret, const SString &siaPath, const SString& filePath) {
  return CEventPtr(new FuseEvent(functionName, ret, siaPath, filePath));
}

#define FUSE_EVENT(func, ret, siaPath, filePath) \
([&](const int& r, const SString& fname, const SString& sp, const SString& fp)->int {\
  if (GetSiaDriveConfig().GetEnableDriveEvents() && ((EventLevelFromString(GetSiaDriveConfig().GetEventLevel()) >= EventLevel::Debug) || (r != 0))) {\
    CEventSystem::EventSystem.NotifyEvent(CreateFuseEvent(fname, r, sp, fp));\
  }\
  return r;\
})(ret, func, siaPath, filePath);

class Fuse {
  private:
    Fuse() {
      /*auto testFunc = [](const SString& path, const bool& directory, AccessData accessData, const OpenData& openData, const bool& created)->bool {
        std::vector<AccessData> parents = _vfs->GetParents(path, directory);
        return true;
      };*/
    }

    ~Fuse() {
    }

  private:
    /*class DirectoryIter {
      public:
        DirectoryIter(const FilePath &filePath, const SString &siaPath) :
          _filePath(filePath),
          _siaPath(siaPath) {
          CSiaFileTreePtr fileTree = GetFileTree();
          const auto dirs = fileTree->QueryDirectories(siaPath);
          for (const auto &dir : dirs) {
            FilePath fp(GetSiaDriveConfig().GetCacheFolder(), siaPath);
            fp.Append(dir);
            if (not fp.IsDirectory()) {
              fp.MakeDirectory();
            }
          }
          const auto files = fileTree->Query(CSiaApi::FormatToSiaPath(FilePath(siaPath, "*")));
          for (const auto &file : files) {
            FilePath fp(GetSiaDriveConfig().GetCacheFolder(), file->GetSiaPath());
            if (not fp.IsFile()) {
              fp.CreateEmptyFile();
            }
          }
          _dir = opendir(&filePath[0]);
        }

        ~DirectoryIter() {
          closedir(_dir);
        }

      private:
        const FilePath _filePath;
        const SString _siaPath;
        DIR *_dir = nullptr;
        off_t _offset = 0;
        struct dirent *_curEntry = nullptr;
        
      public:
        void SeekOffset(const off_t &offset) {
          if (offset != _offset) {
            _offset = offset;
          }
          seekdir(_dir, _offset);
          _curEntry = readdir(_dir);
        }

        void FillBuffer(fuse_fill_dir_t fillerFunc, void *buffer, CSiaFileTreePtr siaFileTree) {
          if (_curEntry) {
            struct stat st{};
            memset(&st, 0, sizeof(st));
            st.st_ino = _curEntry->d_ino;
            st.st_mode = _curEntry->d_type << 12;

            //auto siaFile = siaFileTree ? siaFileTree->GetFile(openFileInfo->GetSiaPath()) : nullptr;
            //st.st_size = siaFile ? siaFile->GetSize() : 0;
            const off_t nextOffset = telldir(_dir);
            if (not fillerFunc(buffer, _curEntry->d_name, &st, nextOffset)) {
              _curEntry = nullptr;
              _offset = nextOffset;
              FillBuffer(fillerFunc, buffer, siaFileTree);
            }
          }
        }
    };
    */
  private:
    static struct fuse_operations _fuseOps;
    static SString _mountLocation;
    static std::unique_ptr<std::thread> _mountThread;
    static std::unique_ptr<VirtualFileSystem> _vfs;
    static int _mountResult;
    static bool _mountActive;
    static bool _mounted;
    static std::shared_ptr<CSiaApi> _siaApi;
    static ITransferManager* _transferManager;

  private:
    static inline CSiaFileTreePtr GetFileTree() {
      CSiaFileTreePtr fileTree;
      _siaApi->GetRenter().GetFileTree(fileTree);
      return fileTree;
    }

    static inline CSiaDriveConfig& GetSiaDriveConfig() {
      return *_siaApi->GetSiaDriveConfig();
    }

    static inline bool GetMountActive() {
      return _mountActive;
    }

    static inline bool GetMounted() {
      return _mounted;
    }

    static inline FilePath MountPathToCachePath(const SString &path) {
      return FilePath(GetSiaDriveConfig().GetCacheFolder(), RemoveMountedLocation(path)).MakeAbsolute();
    }

    static inline FilePath RemoveMountedLocation(const SString &path) {
      return path.SubString(1);
    }

    static inline void SetMountActive(const bool& active) {
      _mountActive = active;
    }

    static inline void SetMounted(const bool& mounted) {
      _mounted = mounted;
    }

  private:
    static int Sia_access(const char *path, int mode) {
      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));
      int ret = -EIO;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else {
        AccessData accessData{};
        if (_vfs->GetAccessData(siaPath, accessData)) {
          if (mode & F_OK) {
            ret = 0;
          } else {
            bool allowed = true;

            if (mode & X_OK) {
              allowed = VFSUtils::CheckUserGroupAccess(Perm::Execute, accessData);
              if (not allowed) {
                ret = -EPERM;
              }
            }

            if (allowed) {
              if (mode == 0) {
                allowed = false;
              }

              if (allowed && (mode & R_OK)) {
                allowed = VFSUtils::CheckUserGroupAccess(Perm::Read, accessData);
              }

              if (allowed && (mode & W_OK)) {
                allowed = VFSUtils::CheckUserGroupAccess(Perm::Write, accessData);
              }

              if (not allowed) {
                ret = -EACCES;
              }
            }
          }
        } else {
          ret = -ENOENT;
        }
      }

      return FUSE_EVENT(__FUNCTION__, ret, siaPath, fp);
    }

#ifdef __APPLE__
    static int Sia_chflags(const char *path, uint32_t flags) {
      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));

      int ret = -EIO;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else if (CreateStubIfMissing(siaPath, fp)) {
        ret = chflags(&fp[0], flags);
      }

      return FUSE_EVENT(__FUNCTION__, (ret == -1) ? -errno : ret, siaPath, fp);
    }
#endif

    static int Sia_chmod(const char *path, mode_t mode) {
      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));
      int ret = -EIO;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else {
        AccessData accessData{};
        if (_vfs->GetAccessData(path, accessData)) {
          if (accessData.UID == getuid()) {
            accessData.Mode = mode;
            if (_vfs->SetAccessData(path, accessData)) {
              ret = 0;
            } else {
              ret = -EIO;
            }
          } else {
            ret = -EACCES;
          }
        } else {
          ret = -ENOENT;
        }
      }

      return FUSE_EVENT(__FUNCTION__, ret, siaPath, fp);
    }

    static int Sia_chown(const char *path, uid_t uid, gid_t gid) {
      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));
      int ret = -EIO;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else {
        AccessData accessData{};
        if (_vfs->GetAccessData(path, accessData)) {
          if (accessData.UID == getuid()) {
            if ((uid == -1) && (gid == -1)) {
              ret = 0;
            } else {
              if (uid != -1) {
                accessData.UID = uid;
              }
              if (gid != -1) {
                accessData.GID = gid;
              }
              if (_vfs->SetAccessData(path, accessData)) {
                ret = 0;
              } else {
                ret = -EIO;
              }
            }
          } else {
            ret = -EACCES;
          }
        } else {
          ret = -ENOENT;
        }
      }

      return FUSE_EVENT(__FUNCTION__, ret, siaPath, fp);
    }

    static int Sia_create(const char *path, mode_t mode, struct fuse_file_info *fi) {
      fi->fh = static_cast<uint64_t>(VFSInvalidHandle);

      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));

      CSiaFileTreePtr fileTree = GetFileTree();
      int ret = 0;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else {
        const bool directoryOp = ((fi->flags & O_DIRECTORY) == O_DIRECTORY);
        const bool mustExist = ((fi->flags & O_CREAT) != O_CREAT);
        const bool truncateOp = ((fi->flags & O_TRUNC) && ((fi->flags & O_WRONLY) || (fi->flags & O_RDWR)));
        if (directoryOp) {
          if (fi->flags & O_CREAT) {
            AccessData accessData{};
            accessData.UID = getuid();
            accessData.GID = getgid();
            accessData.Mode = mode;

            OpenData openData{};
            openData.Flags = fi->flags;
            VirtualFileSystem::VFSItemPtr file;
            const auto ec = _vfs->OpenOrCreate(siaPath, true, accessData, openData, file);
            if (ec == VFSErrorCode::Success) {
              fi->fh = file->GetId();
            }

            ret = VFSUtils::TranslateVFSErrorCode(ec);
          } else if (_vfs->IsDirectory(siaPath)) {
            OpenData openData{};
            openData.Flags = fi->flags;
            VirtualFileSystem::VFSItemPtr file;
            const auto ec = _vfs->Open(siaPath, true, openData, file);
            if (ec == VFSErrorCode::Success) {
              fi->fh = file->GetId();
            }

            ret = VFSUtils::TranslateVFSErrorCode(ec);
          } else {
            ret = -ENOTDIR;
          }
        } else {
          OpenData openData{};
          openData.Flags = fi->flags;

          VirtualFileSystem::VFSItemPtr file;
          if (mustExist) {
            const auto ec = _vfs->Open(siaPath, false, openData, file);
            if (ec == VFSErrorCode::Success) {
              fi->fh = file->GetId();
            }

            ret = VFSUtils::TranslateVFSErrorCode(ec);
          } else {
            AccessData accessData{};
            accessData.UID = getuid();
            accessData.GID = getgid();
            accessData.Mode = mode;
            const auto ec = _vfs->OpenOrCreate(siaPath, false, accessData, openData, file);
            if (ec == VFSErrorCode::Success) {
              fi->fh = file->GetId();
            }

            ret = VFSUtils::TranslateVFSErrorCode(ec);
          }

          if ((ret == 0) && truncateOp) {
            file->Truncate(0);
          }
        }
      }

      return FUSE_EVENT(__FUNCTION__, ret, siaPath, fp);
    }

    static void Sia_destroy(void *private_data) {
      CEventSystem::EventSystem.NotifyEvent(CreateUnMountedEvent(_mountLocation));
    }

#ifdef __APPLE__
    static int Sia_exchange(const char *path1, const char *path2, unsigned long options) {
      const FilePath fp(MountPathToCachePath(path1));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path2));

      const FilePath fp2(MountPathToCachePath(path1));
      const SString siaPath2 = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path2));

      int ret = -EIO;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else {
        uint64_t fh = -1;
        if (AddFileToCache(siaPath, fp) &&
            AddFileToCache(fp2, siaPath2)) {
          ret = exchangedata(&fp[0], &fp[1], options);
          if (ret != -1) {
            SetOpenFileChanged(siaPath);
            SetOpenFileChanged(siaPath2);
          }
        }
      }

      return FUSE_EVENT(__FUNCTION__, (ret == -1) ? -errno : ret, siaPath, fp);
    }
#endif

    static int Sia_fallocate(const char *path, int mode, off_t offset, off_t length, struct fuse_file_info *fi) {
      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));
      int ret = -EIO;

      /*ExecuteHandleOperation(siaPath, fp, fi->fh, [&](std::uint64_t handle, bool& fileChanged, const FuseFileData& fileData) {
        if (not CheckOpenFlags(fileData.Flags, true)) {
          ret = -EACCES;
        } else {
          const auto origSize = FilePath::FileSize(fp);
#ifdef __APPLE__
          fstore_t fstore = {0};

          if (not(mode & PREALLOCATE)) {
            ret = -ENOTSUP;
          } else {
            if (mode & ALLOCATECONTIG) {
              fstore.fst_flags |= F_ALLOCATECONTIG;
            }

            if (mode & ALLOCATEALL) {
              fstore.fst_flags |= F_ALLOCATEALL;
            }

            if (mode & ALLOCATEFROMPEOF) {
              fstore.fst_posmode = F_PEOFPOSMODE;
            } else if (mode & ALLOCATEFROMVOL) {
              fstore.fst_posmode = F_VOLPOSMODE;
            }

            fstore.fst_offset = offset;
            fstore.fst_length = length;

            ret = fcntl(handle, F_PREALLOCATE, &fstore);
          }
#else
          ret = fallocate(handle, mode, offset, length);
#endif
          if ((ret != -1) && ((offset + length) != origSize)) {
            fileChanged = true;
          }
        }
      });

      return FUSE_EVENT(__FUNCTION__, ret, siaPath, fp);*/
    }

#ifndef __APPLE__
    static int Sia_flock(const char *path, struct fuse_file_info *fi, int op) {
      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));

      int ret = -ENOENT;
      VirtualFileSystem::VFSItemPtr file;
      if (_vfs->GetOpenItem(fi->fh, file)) {
        file->ReadOperation([&](int handle)->bool {
          ret = flock(handle, op);
          return (ret == 0);
        }, true);
      }

      return FUSE_EVENT(__FUNCTION__, (ret == -1) ? -errno : ret, siaPath, fp);
    }
#endif

    static int Sia_fsync(const char *path, int isDataSync, struct fuse_file_info *fi) {
      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));

      int ret = -ENOENT;
      VirtualFileSystem::VFSItemPtr file;
      if (_vfs->GetOpenItem(fi->fh, file)) {
        file->WriteOperation([&](int handle)->bool {
          // Open flags do not apply - no need for check
#ifdef __APPLE__
          ret = (isDataSync) ? fcntl(handle, F_FULLFSYNC) : fsync(handle);
#else
          ret = (isDataSync) ? fdatasync(handle) : fsync(handle);
#endif
          return (ret == 0);
        });
      }

      return FUSE_EVENT(__FUNCTION__, (ret == -1) ? -errno : ret, siaPath, fp);
    }

    static int Sia_getattr(const char *path, struct stat *st) {
      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));

      int ret = -ENOENT;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else {
        std::uint64_t fid;
        std::int64_t size;
        std::uint64_t date;
        AccessData accessData{};
        bool directory;

        VirtualFileSystem::VFSItemInfoPtr itemInfo;
        if (_vfs->GetItemInfo(path, itemInfo)) {
          struct stat dst{};
          ret = stat(&GetSiaDriveConfig().GetCacheFolder()[0], &dst);

          if (ret == 0) {
            memset(st, 0, sizeof(struct stat));
            st->st_dev = dst.st_dev;
            st->st_rdev = dst.st_rdev;
            st->st_size = size;
            st->st_mtime = date;
            st->st_ctime = date;
            st->st_gid = accessData.GID;
            st->st_ino = fid;
            st->st_mode = (directory ? S_IFDIR : S_IFREG) | accessData.Mode;
            st->st_uid = accessData.UID;
          }
        }
      }

      return FUSE_EVENT(__FUNCTION__, (ret == -1) ? -errno : ret, siaPath, fp);
    }

    static void *Sia_init(struct fuse_conn_info *conn) {
      SetMounted(true);
      //UpdateDriveInformation();
      GetSiaDriveConfig().SetLastMountLocation(_mountLocation);

#ifdef __APPLE__
      FUSE_ENABLE_SETVOLNAME(conn);
      FUSE_ENABLE_XTIMES(conn);
#endif
      CEventSystem::EventSystem.NotifyEvent(CreateMountedEvent(_mountLocation));
      return nullptr;
    }

    static int Sia_mkdir(const char *path, mode_t mode) {
      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));
      int ret = -EIO;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else {
        AccessData accessData{};
        accessData.Mode = mode;
        accessData.UID = getuid();
        accessData.GID = getgid();

        OpenData openData{};
        openData.Flags = O_CREAT|O_DIRECTORY;
        VirtualFileSystem::VFSItemPtr dir;
        const auto ec = _vfs->Create(siaPath, true, accessData, openData, dir);
        if (ec == VFSErrorCode::Success) {
          dir->Close();
        }

        ret = VFSUtils::TranslateVFSErrorCode(ec);
      }

      return FUSE_EVENT(__FUNCTION__, ret, siaPath, fp);
    }

    static inline int Sia_open(const char *path, struct fuse_file_info *fi) {
      return Sia_create(path, 0, fi);
    }

    static int Sia_opendir(const char *path, struct fuse_file_info *fi) {
      /*const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));
      int ret = -EIO;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else if (CreateStubIfMissing(siaPath, fp)) {
        auto *iter = new DirectoryIter(fp, siaPath);
        fi->fh = reinterpret_cast<std::uint64_t>(iter);
        ret = 0;
      }

      return FUSE_EVENT(__FUNCTION__, ret, siaPath, fp);*/
    }

    static int Sia_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
      /*const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));

      int ret = -EIO;
      ExecuteHandleOperation(siaPath, fp, 0, [&](std::uint64_t handle, bool& wasChanged, const FuseFileData& fileData) {
        if (not CheckOpenFlags(fileData.Flags, false)) {
          ret = -EACCES;
        } else {
          std::uint64_t bytesRead;
          if (ReadBytes(siaPath, buf, size, offset, bytesRead)) {
            ret = bytesRead;
          } else {
            ret = -errno;
          }
        }
      });

      return FUSE_EVENT(__FUNCTION__, (ret == -1) ? -errno : ret, siaPath, fp);
    }

    static int Sia_read_buf(const char *path, struct fuse_bufvec **bufp, size_t size, off_t offset, struct fuse_file_info *fi) {
      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));
      std::cout << "read_buf " << &fp[0] << " " << size << " " << offset << std::endl;
      int ret = -EIO;
      ExecuteHandleReadOperation(siaPath, fp, fi->fh, [&](std::uint64_t handle, bool& wasChanged, const FuseFileData& fileData) {
        ret = -ENOMEM;
        auto *src = (struct fuse_bufvec *) malloc(sizeof(struct fuse_bufvec));
        if (src != nullptr) {
          *src = FUSE_BUFVEC_INIT(size);
          *((int *) &src->buf[0].flags) = 0;
          src->buf[0].mem = malloc(size);
          src->buf[0].size = size;
          std::uint64_t bytesRead;
          if (ReadBytes(siaPath, (char*)src->buf[0].mem, size, offset, bytesRead)) {
            ret = 0;
            *bufp = src;
          } else {
            ret = -errno;
          }
        }
      }, offset, size);

      return FUSE_EVENT(__FUNCTION__, ret, siaPath, fp);*/
    }

    static int Sia_readdir(const char *path, void *buf, fuse_fill_dir_t filler, off_t offset, struct fuse_file_info *fi) {
      /*const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));
      auto *iter = reinterpret_cast<DirectoryIter *>(fi->fh);
      iter->SeekOffset(offset);
      iter->FillBuffer(filler, buf, GetFileTree());

      return 0;*/
    }

    static int Sia_release(const char *path, struct fuse_file_info *fi) {
      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));

      VirtualFileSystem::VFSItemPtr file;
      if (_vfs->GetOpenItem(fi->fh, file)) {
        file->Close();
      }

      return FUSE_EVENT(__FUNCTION__, 0, siaPath, fp);
    }

    static int Sia_releasedir(const char *path, struct fuse_file_info *fi) {
      /*auto *iter = reinterpret_cast<DirectoryIter *>(fi->fh);
      delete iter;*/

      return 0;
    }

    static int Sia_rename(const char *from, const char *to) {
      /*const FilePath fp(MountPathToCachePath(from));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(from));
      const bool &isDir = fp.IsDirectory();
      const FilePath fp2 = MountPathToCachePath(to);
      const SString newSiaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(to));
      int ret = -EIO;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else if (isDir) {
        ret = rename(&fp[0], &fp2[0]);
        if (ret == 0) {
          if (GetFileTree()->DirectoryExists(siaPath)) {
            auto result = GetUploadManager().RenameFolder(siaPath, newSiaPath);
            if (not ApiSuccess(result)) {
              ret = -EIO;
            }
          }
        }
      } else if (GetFileTree()->FileExists(siaPath)) {
        ret = rename(&fp[0], &fp2[0]);
        if (ret == 0) {
          auto result = GetUploadManager().RenameFile(siaPath, newSiaPath);
          if (not ApiSuccess(result)) {
            ret = -EIO;
          }
        }
      } else {
        ret = rename(&fp[0], &fp2[0]);
        if (ret == 0) {
          auto result = GetUploadManager().AddOrUpdate(newSiaPath, fp2, fp2.GetModifiedTime());
          if (not ApiSuccess(result)) {
            ret = -EIO;
          }
        }
      }

      return FUSE_EVENT(__FUNCTION__, (ret == -1) ? -errno : ret, siaPath, fp);*/
    }

    static int Sia_rmdir(const char *path) {
      /*const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));

      int ret = -EIO;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else {
        if (GetFileTree()->DirectoryExists(siaPath)) {
          ret = -ENOTEMPTY;
        } else {
          ret = rmdir(&fp[0]);
          if (ret == 0) {
            GetUploadManager().DirectoryRemoved(siaPath, fp);
          }
        }
      }

      return FUSE_EVENT(__FUNCTION__, (ret == -1) ? -errno : ret, siaPath, fp);*/
    }

#ifdef __APPLE__
    static int Sia_setbkuptime(const char *path, const struct timespec *bkuptime) {
      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));

      int ret = -EIO;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else if (CreateStubIfMissing(siaPath, fp)) {
        struct attrlist attributes;
        attributes.bitmapcount = ATTR_BIT_MAP_COUNT;
        attributes.reserved = 0;
        attributes.commonattr = ATTR_CMN_BKUPTIME;
        attributes.dirattr = 0;
        attributes.fileattr = 0;
        attributes.forkattr = 0;
        attributes.volattr = 0;

        ret = setattrlist(&fp[0], &attributes, (void *) bkuptime, sizeof(struct timespec), FSOPT_NOFOLLOW);
      }

      return FUSE_EVENT(__FUNCTION__, (ret == -1) ? -errno : ret, siaPath, fp);
    }

    static int Sia_setchgtime(const char *path, const struct timespec *chgtime) {
      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));

      int ret = -EIO;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else if (CreateStubIfMissing(siaPath, fp)) {
        struct attrlist attributes;
        attributes.bitmapcount = ATTR_BIT_MAP_COUNT;
        attributes.reserved = 0;
        attributes.commonattr = ATTR_CMN_CHGTIME;
        attributes.dirattr = 0;
        attributes.fileattr = 0;
        attributes.forkattr = 0;
        attributes.volattr = 0;

        ret = setattrlist(&fp[0], &attributes, (void *) chgtime, sizeof(struct timespec), FSOPT_NOFOLLOW);
      }

      return FUSE_EVENT(__FUNCTION__, (ret == -1) ? -errno : ret, siaPath, fp);
    }

    static int Sia_setcrtime(const char *path, const struct timespec *crtime) {
      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));

      int ret = -EIO;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else if (CreateStubIfMissing(siaPath, fp)) {
        struct attrlist attributes;
        attributes.bitmapcount = ATTR_BIT_MAP_COUNT;
        attributes.reserved = 0;
        attributes.commonattr = ATTR_CMN_CRTIME;
        attributes.dirattr = 0;
        attributes.fileattr = 0;
        attributes.forkattr = 0;
        attributes.volattr = 0;

        ret = setattrlist(&fp[0], &attributes, (void *) crtime, sizeof(struct timespec), FSOPT_NOFOLLOW);
      }

      return FUSE_EVENT(__FUNCTION__, (ret == -1) ? -errno : ret, siaPath, fp);
    }
#endif

    static int Sia_statfs(const char *path, struct statvfs *stbuf) {
      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));
      int ret = -EIO;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else {
        ret = statvfs(&GetSiaDriveConfig().GetCacheFolder()[0], stbuf);
        if (ret == 0) {
          SiaCurrency totalBytes;
          _siaApi->GetRenter().CalculateEstimatedStorage(_siaApi->GetRenter().GetFunds(), totalBytes);
          auto usedBlocks = _siaApi->GetRenter().GetTotalUsedBytes() / stbuf->f_frsize;
          stbuf->f_blocks = totalBytes.ToUInt() / stbuf->f_frsize;
          stbuf->f_bavail = stbuf->f_bfree = (stbuf->f_blocks - usedBlocks);
        }
      }

      return FUSE_EVENT(__FUNCTION__, (ret == -1) ? -errno : ret, siaPath, fp);
    }

    static int Sia_truncate(const char *path, off_t size) {
      /*const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));
      int ret = -EIO;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else {
        RequiredHandleOperation(siaPath, fp, [&](std::uint64_t handle, bool &wasChanged, const FuseFileData& fileData) {
          ret = -EACCES;
          if (CheckOpenFlags(fileData.Flags, true)) {
            ret = 0;
            const auto origSize = FilePath::FileSize(fp);
            if (size > origSize) {
              if (not CheckCachePolicy(fp, size)) {
                ret = -EIO;
              }
            }
            if (ret == 0) {
              ret = ftruncate(handle, size);
              if (ret != -1) {
                wasChanged = (size != origSize);
              }
            }
          }
        });
      }

      return FUSE_EVENT(__FUNCTION__, (ret == -1) ? -errno : ret, siaPath, fp);*/
    }

    static int Sia_unlink(const char *path) {
      /*const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));
      int ret = -EIO;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else {
        std::lock_guard<std::mutex> l(_fuseMutex);
        GetUploadManager().SetRenameActive(true);
        ret = unlink(&fp[0]);
        if (ret == 0) {
          ret = ApiSuccess(GetUploadManager().Remove(siaPath)) ? 0 : -EIO;
        }
        GetUploadManager().SetRenameActive(false);
      }

      return FUSE_EVENT(__FUNCTION__, (ret == -1) ? -errno : ret, siaPath, fp);*/
    }

#ifndef __APPLE__
    static int Sia_utimens(const char *path, const struct timespec ts[2]) {
      const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));
      /*int ret = -EIO;
      if (not _vfs->IsFileAllowed(siaPath)) {
        ret = -EACCES;
      } else if (CreateStubIfMissing(siaPath, fp)) {
        ret = utimensat(0, &fp[0], ts, AT_SYMLINK_NOFOLLOW);
      }

      return FUSE_EVENT(__FUNCTION__, (ret == -1) ? -errno : ret, siaPath, fp);*/
    }

#endif
#ifdef __APPLE__
    static int Sia_setvolname(const char *volname) {
      (void)volname;
      return 0;
    }
#endif

    static int Sia_write(const char *path, const char *buf, size_t size, off_t offset, struct fuse_file_info *fi) {
      /*const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));

      int ret = -EIO;
      if (CheckCachePolicy(fp, size)) {
        ExecuteHandleOperation(siaPath, fp, 0, [&](std::uint64_t handle, bool& wasChanged, const FuseFileData& fileData) {
          if (not CheckOpenFlags(fileData.Flags, true)) {
            ret = -EACCES;
          } else {
            std::uint64_t bytesWritten;
            if (WriteBytes(siaPath, buf, size, offset, bytesWritten)) {
              ret = bytesWritten;
            } else {
              ret = -errno;
            }
          }
        });
      }

      return FUSE_EVENT(__FUNCTION__, (ret == -1) ? -errno : ret, siaPath, fp);*/
    }

    static int Sia_write_buf(const char *path, struct fuse_bufvec *buf, off_t offset, struct fuse_file_info *fi) {
      /*const FilePath fp(MountPathToCachePath(path));
      const SString siaPath = CSiaApi::FormatToSiaPath(RemoveMountedLocation(path));

      int ret = -EIO;
      ExecuteHandleOperation(siaPath, fp, fi->fh, [&](std::uint64_t handle, bool& wasChanged, const FuseFileData& fileData) {
        if (not CheckOpenFlags(fileData.Flags, true)) {
          ret = -EACCES;
        } else if (CheckCachePolicy(fp, buf->buf[0].size)) {
          struct fuse_bufvec dst = FUSE_BUFVEC_INIT(fuse_buf_size(buf));
          *((int *) &dst.buf[0].flags) = FUSE_BUF_IS_FD | FUSE_BUF_FD_SEEK;
          dst.buf[0].fd = handle;
          dst.buf[0].pos = offset;
          ret = fuse_buf_copy(&dst, buf, FUSE_BUF_SPLICE_NONBLOCK);
          if (ret >= 0) {
            wasChanged = true;
          }
        }
      });

      return ret;*/
    }

  public:
    static void Initialize(std::shared_ptr<CSiaApi> siaApi, ITransferManager *transferManager) {
      _transferManager = transferManager;
      _siaApi = siaApi;
      _vfs = std::make_unique<VirtualFileSystem>(
        transferManager,
        _siaApi->GetSiaDriveConfig()->GetCacheFolder(),
        VFS_ACCESS_CHECKER,
        VFS_CREATOR,
        VFS_CLOSER,
        _siaApi->GetSiaDriveConfig()->GetVFS_SQLiteDbFilePath(),
        SQLite::OPEN_READWRITE|SQLite::OPEN_CREATE);

      AccessData accessData{};
      accessData.Mode = 0750l;
      accessData.UID = getuid();
      accessData.GID = getgid();

      OpenData openData{};
      openData.Flags = O_CREAT|O_DIRECTORY;
      VirtualFileSystem::VFSItemPtr rootDir;
      const auto ec = _vfs->OpenOrCreate("/", true, accessData, openData, rootDir);
      if (ec == VFSErrorCode::Success) {
        rootDir->Close();
      } else {
        throw StartupException("Failed to create root directory: " + SString::FromInt32(VFSUtils::TranslateVFSErrorCode(ec)));
      }

      memset(&_fuseOps, 0, sizeof(_fuseOps));
      _fuseOps.access = Sia_access;           //TEST POS 100%
      _fuseOps.bmap = nullptr;
      _fuseOps.chmod = Sia_chmod;             //TEST POS 100%
      _fuseOps.chown = Sia_chown;             //TEST POS 100%
      _fuseOps.create = Sia_create;           //TEST POS 100%
      _fuseOps.destroy = Sia_destroy;
      _fuseOps.fallocate = Sia_fallocate;     //TEST POS 100%
#ifndef __APPLE__
      _fuseOps.flock = Sia_flock;             //TEST POS 66%
#endif
      _fuseOps.flush = nullptr;
      _fuseOps.fsync = Sia_fsync;             //TEST POS 100%
      _fuseOps.fsyncdir = nullptr;
      _fuseOps.getattr = Sia_getattr;
      _fuseOps.getxattr = nullptr;
      _fuseOps.init = Sia_init;
      _fuseOps.ioctl = nullptr;
      _fuseOps.link = nullptr;
      _fuseOps.listxattr = nullptr;
#ifndef __APPLE__
      _fuseOps.lock = nullptr;
#endif
      _fuseOps.mkdir = Sia_mkdir;             //TEST POS 100%
      _fuseOps.mknod = nullptr;
      _fuseOps.open = Sia_open;               //TEST POS 100%
      _fuseOps.opendir = Sia_opendir;
      _fuseOps.poll = nullptr;
      _fuseOps.read = Sia_read;
      //_fuseOps.read_buf = Sia_read_buf;
      _fuseOps.readdir = Sia_readdir;
      _fuseOps.readlink = nullptr;
      _fuseOps.release = Sia_release;
      _fuseOps.releasedir = Sia_releasedir;
      _fuseOps.removexattr = nullptr;
      _fuseOps.rename = Sia_rename;           //TEST POS 33%
      _fuseOps.rmdir = Sia_rmdir; // TODO Revisit
      _fuseOps.setxattr = nullptr;
      _fuseOps.statfs = Sia_statfs;
      _fuseOps.symlink = nullptr;
      _fuseOps.truncate = Sia_truncate;
      _fuseOps.unlink = Sia_unlink;           //TEST POS 100%
#ifndef __APPLE__
      _fuseOps.utimens = Sia_utimens;
#endif
      _fuseOps.write = Sia_write;
      _fuseOps.write_buf = Sia_write_buf;     // TEST POS 100%
#ifdef __APPLE__
      _fuseOps.chflags = Sia_chflags;
      _fuseOps.exchange = Sia_exchange;
      _fuseOps.fsetattr_x = nullptr;
      _fuseOps.getxtimes = Sia_getxtimes;
      _fuseOps.setattr_x = nullptr;
      _fuseOps.setbkuptime = Sia_setbkuptime;
      _fuseOps.setchgtime = Sia_setchgtime;
      _fuseOps.setcrtime = Sia_setcrtime;
      _fuseOps.setvolname = Sia_setvolname;
#endif
    }

    static inline bool IsConfigured() {
      return (_vfs != nullptr);
    }

    static void Mount(const std::vector<std::string>& args) {
      if (not _mountThread) {
        _mountResult = 0;
        SetMountActive(true);
        _mountThread = std::make_unique<std::thread>([&]() {
          umask(0);
          std::vector<const char *> mountArgs({"siadrive",
                                          "-ofsname=SiaDrive",
                                          "-oentry_timeout=0",
                                          "-onegative_timeout=0",
                                          "-oattr_timeout=0"});
          for (int i = 1; i < args.size(); i++) {
            auto it = std::find_if(args.begin(), args.end(), [&](const auto& arg)->bool {
              auto match = (std::string(arg).find(args[i]) == 0);
              if (not match) {
                const SString a1(arg);
                const SString a2(args[i]);
                const auto a1s = a1.Split('=');
                const auto a2s = a2.Split('=');
                if ((a1s.size() == 2) && (a2s.size() == 2)) {
                  match = (a1s[0] == a2s[0]);
                }
              }
              return match;
            });

            if (it == args.end()) {
              mountArgs.emplace_back(args[i].c_str());
            }

            if (args[i][0] != '-') {
              _mountLocation = args[i];
            }
          }

          struct fuse_args fuseArgs = FUSE_ARGS_INIT(static_cast<int>(mountArgs.size()), (char**)&mountArgs[0]);
          _mountResult = fuse_main(fuseArgs.argc, fuseArgs.argv, &_fuseOps, nullptr);
          SetMountActive(false);
          SetMounted(false);
          CEventSystem::EventSystem.NotifyEvent(CreateMountEndedEvent(_mountLocation, _mountResult));
        });
      }
    }

    static void Unmount() {
      if (_mountThread) {
        while (GetMountActive() && not GetMounted()) {
          std::this_thread::sleep_for(10ms);
        }

        // TODO Wait for 30 seconds and then notify unmount failed due to files in use
        const auto checkMounted = "mount | grep \"" + _mountLocation + "\" >/dev/null 2>&1";
        while (system(&checkMounted[0]) == 0) {
          std::this_thread::sleep_for(10ms);
#ifdef __APPLE__
          const SString &unmount = "umount \"" + _mountLocation + "\" >/dev/null 2>&1";
#else
          const SString &unmount = "fusermount -u \"" + _mountLocation + "\" >/dev/null 2>&1";
#endif
          system(&unmount[0]);
        }
        _mountThread->join();
        _mountLocation = "";
        _mountThread.reset();
        _mountActive = false;
      }
    }

    static bool IsMounted() {
      return GetMountActive();
    }

    static void NotifyOnline() {
      if (IsConfigured()) {
        _transferManager->Start();
      }
    }

    static void NotifyOffline() {
      if (IsConfigured()) {
        _transferManager->Stop();
      }
    }

    static void Shutdown() {
      Unmount();
      _vfs.reset();
      memset(&_fuseOps, 0, sizeof(_fuseOps));
    }

    static int GetMountResult() {
      return _mountResult;
    }

    static void WaitForMount() {
      const auto checkMount = "mount | grep \"" + _mountLocation + "\" >/dev/null 2>&1";
      for (int i = 0; i < 3000 && (system(&checkMount[0]) != 0); i++) {
        std::this_thread::sleep_for(10ms);
      }
    }
};
struct fuse_operations Fuse::_fuseOps{};
SString Fuse::_mountLocation;
std::unique_ptr<std::thread> Fuse::_mountThread;
int Fuse::_mountResult = 0;
bool Fuse::_mountActive = false;
bool Fuse::_mounted = false;
std::unique_ptr<VirtualFileSystem> Fuse::_vfs;
ITransferManager* Fuse::_transferManager = nullptr;
std::shared_ptr<CSiaApi> Fuse::_siaApi;


CFuseDrive::CFuseDrive(std::shared_ptr<CSiaApi> siaApi, ITransferManager *transferManager) {
  // Restrict mutex scope
  {
    std::lock_guard<std::mutex> l(_startStopMutex);
    if (Fuse::IsConfigured()) {
      throw FuseDriveException("Sia drive has already been activated");
    }
    Fuse::Initialize(std::move(siaApi), transferManager);
  }
  CEventSystem::EventSystem.AddEventConsumer([this](const CEvent &event) {
    if (event.GetEventName() == "SystemCriticalEvent") {
      Unmount();
    }
  });
}

CFuseDrive::~CFuseDrive() {
  std::lock_guard<std::mutex> l(_startStopMutex);
  Fuse::Shutdown();
}

void CFuseDrive::ClearCache() {
  std::lock_guard<std::mutex> l(_startStopMutex);
}

bool CFuseDrive::IsMounted() const {
  return Fuse::IsMounted();
}

void CFuseDrive::Mount(const std::vector<std::string>& args) {
  std::lock_guard<std::mutex> l(_startStopMutex);
  Fuse::Mount(args);
  Fuse::WaitForMount();
}

void CFuseDrive::NotifyOnline() {
  std::lock_guard<std::mutex> l(_startStopMutex);
  Fuse::NotifyOnline();
}

void CFuseDrive::NotifyOffline() {
  std::lock_guard<std::mutex> l(_startStopMutex);
  Fuse::NotifyOffline();
}

void CFuseDrive::Unmount(const bool &clearCache) {
  std::lock_guard<std::mutex> l(_startStopMutex);
  Fuse::Unmount();
}

void CFuseDrive::Shutdown() {
  std::lock_guard<std::mutex> l(_startStopMutex);
  Fuse::Shutdown();
}

int CFuseDrive::GetMountResult() const {
  return Fuse::GetMountResult();
}
