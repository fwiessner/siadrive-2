#ifdef __APPLE__
#include "mac_platform.h"
#include <iostream>
#import <Cocoa/Cocoa.h>
#import <Foundation/Foundation.h>
#include <filepath.h>
#include <base64.h>

void setProtectedData(const std::string& key, const std::string& value) {
  std::string urlKey = "http://" + key + ".siadrive.app";

  NSURL *url = [NSURL URLWithString:[NSString stringWithCString:urlKey.c_str() encoding:[NSString defaultCStringEncoding]]];
  NSURLProtectionSpace* protectionSpace = [[NSURLProtectionSpace alloc] initWithHost:url.host
                                            port:[url.port integerValue]
                                            protocol:url.scheme
                                            realm:nil
                                            authenticationMethod:NSURLAuthenticationMethodHTTPDigest];

  NSURLCredential *credential = [NSURLCredential credentialWithUser:[NSString stringWithCString:key.c_str() encoding:[NSString defaultCStringEncoding]] password:[NSString stringWithCString:value.c_str() encoding:[NSString defaultCStringEncoding]] persistence:NSURLCredentialPersistencePermanent];
  [[NSURLCredentialStorage sharedCredentialStorage] setCredential:credential forProtectionSpace:protectionSpace];
}

std::string getProtectedData(const std::string& key) {
  std::string ret;
  std::string urlKey = "http://" + key + ".siadrive.app";

  NSURL *url = [NSURL URLWithString:[NSString stringWithCString:urlKey.c_str() encoding:[NSString defaultCStringEncoding]]];
  NSURLProtectionSpace* protectionSpace = [[NSURLProtectionSpace alloc] initWithHost:url.host
                                            port:[url.port integerValue]
                                            protocol:url.scheme
                                            realm:nil
                                            authenticationMethod:NSURLAuthenticationMethodHTTPDigest];

  NSDictionary* credentials = [[NSURLCredentialStorage sharedCredentialStorage] credentialsForProtectionSpace:protectionSpace];
  if (credentials) {
    NSURLCredential* credential = [credentials.objectEnumerator nextObject];
    if (credential) {
      ret = std::string([[credential password] UTF8String]);
    }
  }

  return ret;
}

std::string getResourcesUrl() {
    NSBundle* mainBundle;
    mainBundle = [NSBundle mainBundle];
    return std::string([[[mainBundle resourceURL] absoluteString] UTF8String]);
}

std::string getResourcesDir() {
    NSBundle* mainBundle;
    mainBundle = [NSBundle mainBundle];
    return std::string([[[mainBundle resourceURL] path] UTF8String]);
}

bool processIsRunning(const std::string& process) {
  std::string p = "/" + process;

  NSString *output = [NSString string];
  for (NSRunningApplication *app in [[NSWorkspace sharedWorkspace] runningApplications]) {
    if ([[[app bundleURL] absoluteString] hasSuffix:[NSString stringWithCString:p.c_str() encoding:[NSString defaultCStringEncoding]]]) {
      return true;
    }
  }

  return false;
}

bool processIsRunningFullPath(const std::string& process) {
  std::string p = process;

  NSString *output = [NSString string];
  for (NSRunningApplication *app in [[NSWorkspace sharedWorkspace] runningApplications]) {
    if ([[[app bundleURL] absoluteString] hasPrefix:[NSString stringWithCString:p.c_str() encoding:[NSString defaultCStringEncoding]]]) {
      return true;
    }
  }

  return false;
}

#endif