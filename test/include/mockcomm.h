#ifndef _MOCKCOMM_H
#define _MOCKCOMM_H

#include "gmock/gmock.h"
#include <siacommon.h>
#include <fstream>
#include <filepath.h>

NS_BEGIN(Sia) NS_BEGIN(Api)

class MockSiaComm :
  public virtual ISiaApiCommunicator {
  public:
    MOCK_CONST_METHOD0(GetServerVersion, SString());

    MOCK_CONST_METHOD2(Get, SiaCommError(
      const SString &path, json
      &result));

    MOCK_CONST_METHOD3(Get, SiaCommError(
      const SString &path,
      const HttpParameters &parameters, json
      &result));

    MOCK_CONST_METHOD3(Post, SiaCommError(
      const SString &path,
      const HttpParameters &parameters, json
      &response));
};

NS_END(2)
#endif