#ifndef SIADRIVE_MOCKTRANSFERMANAGER_H
#define SIADRIVE_MOCKTRANSFERMANAGER_H

#include <siatransfermanager.h>

NS_BEGIN(Sia)
NS_BEGIN(Api)

class CMockTransferManager :
  public virtual ITransferManager {
  public:
    virtual ~CMockTransferManager() {
    }

  public:
    MOCK_METHOD2(Download, bool(const SString &path, const SString &destPath));
    MOCK_METHOD1(Remove, bool(const SString &path));
    MOCK_METHOD2(Upload, bool(const SString &path, const SString &filePath));
};

NS_END(2)
#endif //SIADRIVE_MOCKTRANSFERMANAGER_H
