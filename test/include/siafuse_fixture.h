#ifdef __linux__
#ifndef SIADRIVE_SIAFUSE_FIXTURE_H
#define SIADRIVE_SIAFUSE_FIXTURE_H
#if 0
#include "unittestcommon.h"
#include "mockcomm.h"
#include <siafusedrive.h>
#include <uploadmanager.h>
#include "mocktransfermanager.h"

using namespace Sia::Api::FUSE;
using namespace std::chrono_literals;
NS_BEGIN(Sia) NS_BEGIN(Api)

class SiaFuseTest :
  public ::testing::Test {
  public:
    virtual ~SiaFuseTest() {
      fuseDrive.reset();
      siaApi.reset();
      transferManager.reset();
      mockComm.reset();
    }

  protected:
    std::shared_ptr<MockSiaComm> mockComm;
    std::shared_ptr<CSiaApi> siaApi;
    std::unique_ptr<CSiaFuseDrive> fuseDrive;
    std::unique_ptr<CMockTransferManager> transferManager;

  protected:
    virtual void SetUp() override {
      FilePath("./renter_upload.db3").DeleteAsFile();
      FilePath("./test_cache").DeleteAsDirectory();
      FilePath("./test_mount").DeleteAsDirectory();
      FilePath("./test_mount").MakeDirectory();
      FilePath("./test_cache").MakeDirectory();

      mockComm.reset(new MockSiaComm());
      auto sdc = std::make_shared<CSiaDriveConfig>(false, "./test.json", mockComm);
      sdc->SetCacheFolder("./test_cache");
      sdc->SetLastMountLocation("./test_mount");
      sdc->SetRenter_UploadDbFilePath("./renter_upload.db3");

      siaApi.reset(new CSiaApi(sdc, true));
      transferManager.reset(new CMockTransferManager);
      fuseDrive.reset(new CSiaFuseDrive(siaApi, transferManager.get()));
    }

    virtual void TearDown() override {
      fuseDrive.reset();
      siaApi.reset();
      mockComm.reset();
      transferManager.reset();
    }
};

NS_END(2)
#endif
#endif //SIADRIVE_SIAFUSE_FIXTURE_H
#endif
