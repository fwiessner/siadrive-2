#include <siavfs_fixture.h>

TEST_F(SiaVfsTest, CreateNewFileAndClose) {
  const SString sourcePath = FilePath("./temp_cache/cff8b9ab4134b2cd0b1980bfb308424d1e50566964916c24cea428de243d228c").MakeAbsolute();
  FilePath(sourcePath).DeleteAsFile();

  // Upload for 0-length should not be called
  EXPECT_CALL(_tm, Upload(_, _))
    .Times(0);

  AccessData accessData{};
  OpenData openData{};
#ifdef _WIN32
  openData.DesiredAccess = GENERIC_READ;
  openData.ShareMode = FILE_SHARE_READ;
  accessData.SecAttr = {0};
  openData.CreateDisp = CREATE_ALWAYS;
  openData.Flags = FILE_ATTRIBUTE_NORMAL;
#else
  openData.Flags = O_CREAT;
  accessData.Mode = 0600u;
  accessData.GID = getgid();
  accessData.UID = getuid();
#endif
  TestVFS::VFSItemPtr file;
  ASSERT_EQ(VFSErrorCode::Success, vfs->Create("/test/file.txt", false, accessData, openData, file));
  ASSERT_TRUE(file);
  ASSERT_EQ(1, vfs->GetOpenCount("/test/file.txt"));

  ASSERT_EQ(VFSInvalidHandle, file->GetHandle());
  ASSERT_EQ(getuid(), file->GetAccessData().UID);
  ASSERT_EQ(getgid(), file->GetAccessData().GID);
  ASSERT_EQ(0600u, file->GetAccessData().Mode);
  ASSERT_EQ(O_CREAT, file->GetOpenData().Flags);
  ASSERT_STREQ("/test/file.txt", &file->GetPath()[0]);
  ASSERT_FALSE(file->IsDirectory());
  ASSERT_EQ(0, file->GetSize());
  ASSERT_STREQ(&sourcePath[0], &file->GetSourcePath()[0]);
  ASSERT_EQ(VFSItemStatus::Created, file->GetStatus());
  ASSERT_FALSE(file->IsCached());
  ASSERT_FALSE(FilePath(sourcePath).IsFile());

  file->Close();
  ASSERT_EQ(0, vfs->GetOpenCount("/test/file.txt"));
}

TEST_F(SiaVfsTest, OpenExistingNoConflict) {
  const SString sourcePath = FilePath("./temp_cache/cff8b9ab4134b2cd0b1980bfb308424d1e50566964916c24cea428de243d228c").MakeAbsolute();
  FilePath(sourcePath).DeleteAsFile();

  // Upload for 0-length should not be called
  EXPECT_CALL(_tm, Upload(_, _))
    .Times(0);

  AccessData accessData{};
  OpenData openData{};
#ifdef _WIN32
  openData.DesiredAccess = GENERIC_READ;
  openData.ShareMode = FILE_SHARE_READ;
  accessData.SecAttr = {0};
  openData.CreateDisp = CREATE_ALWAYS;
  openData.Flags = FILE_ATTRIBUTE_NORMAL;
#else
  openData.Flags = O_CREAT|O_RDONLY;
  accessData.Mode = 0600u;
  accessData.GID = getgid();
  accessData.UID = getuid();
#endif
  VirtualFileSystem::VFSItemPtr file;
  ASSERT_EQ(VFSErrorCode::Success, vfs->Create("/test/file.txt", false, accessData, openData, file));

#ifdef _WIN32
  openData.CreateDisp = OPEN_EXISTING;
#else
  openData.Flags = O_RDONLY;
#endif
  TestVFS::VFSItemPtr secondFile;
  ASSERT_EQ(VFSErrorCode::Success, vfs->Open("/test/file.txt", false, openData, secondFile));
  ASSERT_TRUE(secondFile);
  ASSERT_EQ(2, vfs->GetOpenCount("/test/file.txt"));

  ASSERT_EQ(VFSInvalidHandle, secondFile->GetHandle());
  ASSERT_NE(file->GetId(), secondFile->GetId());
  ASSERT_EQ(file->GetStatus(), secondFile->GetStatus());
  ASSERT_EQ(getuid(), secondFile->GetAccessData().UID);
  ASSERT_EQ(getgid(), secondFile->GetAccessData().GID);
  ASSERT_EQ(0600u, secondFile->GetAccessData().Mode);
  ASSERT_EQ(O_RDONLY, secondFile->GetOpenData().Flags);
  ASSERT_STREQ("/test/file.txt", &secondFile->GetPath()[0]);
  ASSERT_FALSE(secondFile->IsDirectory());
  ASSERT_EQ(0, secondFile->GetSize());
  ASSERT_STREQ(&sourcePath[0], &secondFile->GetSourcePath()[0]);
  ASSERT_EQ(VFSItemStatus::Created, secondFile->GetStatus());
  ASSERT_FALSE(secondFile->IsCached());
  ASSERT_FALSE(FilePath(sourcePath).IsFile());

  secondFile->Close();
  ASSERT_EQ(1, vfs->GetOpenCount("/test/file.txt"));

  file->Close();
  ASSERT_EQ(0, vfs->GetOpenCount("/test/file.txt"));
}

TEST_F(SiaVfsTest, WriteToNewFile) {
  const SString sourcePath = FilePath("./temp_cache/cff8b9ab4134b2cd0b1980bfb308424d1e50566964916c24cea428de243d228c").MakeAbsolute();
  FilePath(sourcePath).DeleteAsFile();

  EXPECT_CALL(_tm, Upload(SStrEq("/test/file.txt"), SStrEq(sourcePath)))
    .WillOnce(Return(true));
  EXPECT_CALL(_tm, Remove(SStrEq("/test/file.txt")))
    .WillOnce(Return(true));

  AccessData accessData{};
  OpenData openData{};
#ifdef _WIN32
  openData.DesiredAccess = GENERIC_READ|GENERIC_WRITE;
  openData.ShareMode = FILE_SHARE_READ;
  accessData.SecAttr = {0};
  openData.CreateDisp = CREATE_ALWAYS;
  openData.Flags = FILE_ATTRIBUTE_NORMAL;
#else
  openData.Flags = O_CREAT | O_RDWR;
  accessData.Mode = 0600u;
  accessData.GID = getgid();
  accessData.UID = getuid();
#endif
  TestVFS::VFSItemPtr file;
  ASSERT_EQ(VFSErrorCode::Success, vfs->Create("/test/file.txt", false, accessData, openData, file));

  std::size_t bytesWritten;
  std::vector<char> data = { 'T', 'E', 'S', 'T', '\0' };
  ASSERT_TRUE(file->Write(&data[0], data.size(), 0, bytesWritten));
  ASSERT_NE(VFSInvalidHandle, file->GetHandle());
  ASSERT_EQ(data.size(), bytesWritten);
  ASSERT_EQ(VFSItemStatus::Modified, file->GetStatus());
  ASSERT_EQ(data.size(), file->GetSize());
  ASSERT_TRUE(FilePath(sourcePath).IsFile());

  file->Close();
  ASSERT_EQ(VFSItemStatus::Created, file->GetStatus());
  ASSERT_EQ(VFSInvalidHandle, file->GetHandle());
  ASSERT_TRUE(file->IsCached());
  ASSERT_TRUE(FilePath(sourcePath).IsFile());

  FilePath(sourcePath).DeleteAsFile();
}