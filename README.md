# SiaDrive
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/87720107be1b41229dc1fdcbd389e9cd)](https://www.codacy.com/app/scott.e.graves/SiaDrive?utm_source=siaextensions@bitbucket.org&amp;utm_medium=referral&amp;utm_content=siaextensions/siadrive&amp;utm_campaign=Badge_Grade)

SiaDrive is a user-mode filesystem/storage driver for Windows 7/8/8.1/10 and a FUSE filesystem implementation for Linux/OSX. Mount Sia to any available drive letter on Windows or any directory location on Linux/OSX.

# Current Status
- Most importantly, there are no binaries or packages available. 
- Alpha 5 binaries have been removed and will not be patched or supported any further.
- The legacy UI has been completely removed as a dependency. 
- The Linux FUSE implementation is currently being rewritten as a headless service to support mount-at-boot. 
- OSX will be the second platform to be supported, but should be available within a few weeks of Linux release.
- Windows will be the final platform to be supported and will most likely take ~4-6 weeks to complete.

# Downloads
* COMING SOON

# Build Instructions
[BUILDING](https://bitbucket.org/siaextensions/siadrive/raw/master/BUILDING)

# Linux Details
SiaDrive is designed to mount at boot via fstab and run as root user. This implies that the system administrator is the sole owner of the Sia instance. Additional siad instances can be executed to support a multiuser configuration; however, mounting per-user locations via SiaDrive is not supported. The system administrator is responsible for configuring the mounted location as if it where a normal physical drive. Additionally, only one mount location is currently supported in fstab.

# Linux Locations
- Configuration file (/etc/siadrive/siadriveconfig.json)
- Cache location (/var/cache/siadrive)
- Bundled Sia location (/var/lib/siadrive/sia)
- Bundled Sia data location (/var/lib/siadrive/data)
- Log location (/var/log/siadrive/siadrive.log)

# Important Notes
- It is strongly recommended to use the bundled Sia version; however, SiaDrive will work with existing Sia-UI or siad instances. **If SiaDrive is being used, you should NOT add or remove files using Sia-UI or any other Sia client interface. Sia-UI can be safely used for all other functions.** This may change in future releases. Partial support for synchronizing file changes between different clients has been added to 'siadrived' startup but is not actively running. 
- The wallet needs to be running (and unlocked) at all times, especially near contract renewal period. It's best to run SiaDrive on a computer that is powered-on most of the day.
- It's best to store files that do not change frequently. This is not a requirement but it can become a bit costly. Modified files result in a delete and re-upload to Sia. Future versions of Sia should make this less of an issue as deleted space will be reclaimed on hosts.

# Bugs and Feature Requests
- [Submit Here](https://bitbucket.org/siaextensions/siadrive/issues?status=new&status=open)
- [Email](mailto:sia.extensions@gmail.com)

# Desktop GUI
[SiaDriveUI](https://bitbucket.org/siaextensions/siadriveui)

# Credits
- [Chromium Embedded Framework](https://bitbucket.org/chromiumembedded/cef)
- [Dokany](https://github.com/dokan-dev/dokany)
- [JSON for Modern C++](https://github.com/nlohmann/json)
- [PicoSHA2](https://github.com/okdshin/PicoSHA2)
- [Sia](https://sia.tech/)
- [SQLiteCpp](https://github.com/SRombauts/SQLiteCpp)
- [ThreadPool](https://github.com/progschj/ThreadPool)
- [ttmath](http://www.ttmath.org/)

# Tips
-  SC: 92ab371d2c0613d4379f1b3991873760906073f289ddd38855569639c9a258be0033ff37ad39
- BTC: 18FhNwu4vgun3AZxUav9ncVCCyxfH3H3GN
- ETN: etnkA59iP6XMTC6YudhPc65qfot1WZQRS49X8jaYDB1XS51XTSt51HM9qVnG3d3WjVYjQHmTKa7BUVw85kXAATadA55NoHQip7
- LTC: LhT8nVzmNG1DkJuQepxPTkjX1DneqQy8Y8
- XMR: 44TLBj32YZaiAcx5kXkM1Nf3VUQTkqYQbbZdKAd3php1M9h8TViANguBhRQnCAZeDa3yyrJscGMX9Hcg4nyxbkS1NamoKLP