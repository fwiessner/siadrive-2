# Testing
enable_testing()

set(GTEST_PROJECT_NAME gtest_${GTEST_VERSION})
set(GTEST_BUILD_ROOT ${EXTERNAL_BUILD_ROOT}/builds/${GTEST_PROJECT_NAME})
if (MSVC)
  ExternalProject_Add(gtest_project
    DOWNLOAD_NO_PROGRESS 1
    URL https://github.com/google/googletest/archive/release-${GTEST_VERSION}.tar.gz
    PREFIX ${GTEST_BUILD_ROOT}
    CMAKE_ARGS -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DCMAKE_BUILD_TYPE=${EXTERNAL_BUILD_TYPE} -Dgtest_force_shared_crt=OFF -DBUILD_SHARED_LIBS=ON
    INSTALL_COMMAND ${CMAKE_COMMAND} -E echo "Skipping install step."
    )
else ()
  ExternalProject_Add(gtest_project
    DOWNLOAD_NO_PROGRESS 1
    URL https://github.com/google/googletest/archive/release-${GTEST_VERSION}.tar.gz
    PREFIX ${GTEST_BUILD_ROOT}
    CMAKE_ARGS -DCMAKE_POSITION_INDEPENDENT_CODE=ON -DCMAKE_BUILD_TYPE=${EXTERNAL_BUILD_TYPE}
    INSTALL_COMMAND ${CMAKE_COMMAND} -E echo "Skipping install step."
    )
endif ()
set(GTEST_INCLUDE_DIRS
  ${GTEST_BUILD_ROOT}/src/gtest_project/googletest/include
  ${GTEST_BUILD_ROOT}/src/gtest_project/googlemock/include
  )

if (MSVC)
  if (CMAKE_GENERATOR_LOWER STREQUAL "nmake makefiles")
    set(GTEST_LIBRARIES
      ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gmock.lib
      ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gmock_main.lib
      ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/gtest.lib
      ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/gtest_main.lib
      )
  else ()
    set(GTEST_LIBRARIES
      ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/${CMAKE_BUILD_TYPE}/gmock.lib
      ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/${CMAKE_BUILD_TYPE}/gmock_main.lib
      ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/${CMAKE_BUILD_TYPE}/gtest.lib
      ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/${CMAKE_BUILD_TYPE}/gtest_main.lib
      )
  endif ()
endif ()
if (UNIX)
  set(GTEST_LIBRARIES
    ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/libgmock.a
    ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/libgtest.a
    ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/libgtest_main.a
    )
endif ()

file(GLOB_RECURSE UNITTEST_SOURCES
  ${CMAKE_CURRENT_SOURCE_DIR}/test/src/*.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/test/src/*.cxx
  ${CMAKE_CURRENT_SOURCE_DIR}/test/src/*.c
  )

add_executable(unittests ${UNITTEST_SOURCES})
add_dependencies(unittests gtest_project siadrive.common.api)
target_compile_definitions(unittests PUBLIC GTEST_LINKED_AS_SHARED_LIBRARY=1)
set(UNIT_TEST_LIBRARIES siadrive.common.api ${GTEST_LIBRARIES})
if (MSVC)
  #set(UNIT_TEST_LIBRARIES siadrive.dokany.api ${UNIT_TEST_LIBRARIES})
  #add_dependencies(unittests siadrive.dokany.api)
  CodeSignFile(unittests unittests.exe)
  if (CMAKE_GENERATOR_LOWER STREQUAL "nmake makefiles")
    add_custom_command(TARGET unittests
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gmock.dll ${CMAKE_CURRENT_BINARY_DIR}
      )
    add_custom_command(TARGET unittests
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gmock_main.dll ${CMAKE_CURRENT_BINARY_DIR}
      )
    add_custom_command(TARGET unittests
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/gtest.dll ${CMAKE_CURRENT_BINARY_DIR}
      )
    add_custom_command(TARGET unittests
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/gtest_main.dll ${CMAKE_CURRENT_BINARY_DIR}
      )
  else ()
    add_custom_command(TARGET unittests
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/${CMAKE_BUILD_TYPE}/gmock.dll ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}/
      )
    add_custom_command(TARGET unittests
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/${CMAKE_BUILD_TYPE}/gmock_main.dll ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}/
      )
    add_custom_command(TARGET unittests
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/${CMAKE_BUILD_TYPE}/gtest.dll ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}/
      )
    add_custom_command(TARGET unittests
      POST_BUILD
      COMMAND ${CMAKE_COMMAND} -E copy ${GTEST_BUILD_ROOT}/src/gtest_project-build/googlemock/gtest/${CMAKE_BUILD_TYPE}/gtest_main.dll ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}/
      )
  endif ()
endif ()
if (LINUX)
  set(UNIT_TEST_LIBRARIES siadrive.fuse.api ${UNIT_TEST_LIBRARIES})
  set(COMMON_LIBRARIES ${COMMON_LIBRARIES})
  add_dependencies(unittests siadrive.fuse.api)
endif ()
target_include_directories(unittests PUBLIC
  ${GTEST_INCLUDE_DIRS}
  ${CMAKE_CURRENT_SOURCE_DIR}/test/include)
target_link_libraries(unittests PRIVATE ${UNIT_TEST_LIBRARIES} ${COMMON_LIBRARIES})

add_custom_command(TARGET unittests
  PRE_BUILD
  COMMAND ${CMAKE_COMMAND}
  ARGS -E copy_directory ${CMAKE_CURRENT_SOURCE_DIR}/test/data ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE}/data
  )
add_test(NAME AllTests COMMAND unittests --gtest_output=${gtest_output} WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/${CMAKE_BUILD_TYPE})