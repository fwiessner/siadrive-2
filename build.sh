#!/bin/bash

TARGET_MODE=$1
if [ ! ${TARGET_MODE} ]; then
  echo "Build type not set [Debug/Release]"
  exit 1
fi

realpath() {
  [[ $1 = /* ]] && echo "$1" || echo "$PWD/${1#./}"
}

ABSOLUTE_PATH=$(realpath "$0")
ROOT_PATH=`dirname ${ABSOLUTE_PATH}`
DIST_DIR=${ROOT_PATH}/dist
TARGET_DIR=${DIST_DIR}/${TARGET_MODE}

CMAKE_BASE_VER=3.9
CMAKE_VER=cmake-${CMAKE_BASE_VER}.1
OSMATCHER="^darwin.*$"
if [[ $OSTYPE =~ $OSMATCHER ]]; then
  CMAKE_PLATFORM=Darwin-x86_64
  CMAKE_BASE_NAME=${CMAKE_VER}-${CMAKE_PLATFORM}
  CMAKE_BASE_DIR=${ROOT_PATH}/bin/${CMAKE_BASE_NAME}/CMake.app/Contents
  OSNAME=Darwin
else
  CMAKE_PLATFORM=Linux-x86_64
  CMAKE_BASE_NAME=${CMAKE_VER}-${CMAKE_PLATFORM}
  CMAKE_BASE_DIR=${ROOT_PATH}/bin/${CMAKE_BASE_NAME}
  OSNAME=Linux
fi

CMAKE=${CMAKE_BASE_DIR}/bin/cmake
CTEST=${CMAKE_BASE_DIR}/bin/ctest
PATH=${CMAKE_BASE_DIR}/bin:$PATH
export PATH

cd "${ROOT_PATH}"

if [ ! -f ${CMAKE} ]; then
  rm -rf "bin/cmake-*"
  echo Downloading ${CMAKE_BASE_NAME}
  curl https://cmake.org/files/v${CMAKE_BASE_VER}/${CMAKE_BASE_NAME}.tar.gz -s -k -o bin/${CMAKE_BASE_NAME}.tar.gz || exit 1
  echo Extracting ${CMAKE_BASE_NAME}
  tar xvzf bin/${CMAKE_BASE_NAME}.tar.gz -C bin/ || exit 1
fi

if [ -d "${TARGET_DIR}" ]; then
 echo Cleaning previous build \[dist/${TARGET_MODE}\]
 rm -rf "${TARGET_DIR}"
fi

if [ ! -d "build/${TARGET_MODE}" ]; then
  mkdir -p "build/${TARGET_MODE}"
fi

cd "build/${TARGET_MODE}"
echo Building \[${TARGET_MODE}\]
(${CMAKE} ../.. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=${TARGET_MODE} && make -j4) || (${CMAKE} ../.. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=${TARGET_MODE} && make -j4) || exit 1

echo Testing \[${TARGET_MODE}\]
${CTEST} -V -C ${TARGET_MODE} || exit 1

echo Installing \[${TARGET_MODE}\]
make -j4 install || exit 1
rm -rf "${TARGET_DIR}/htdocs/.idea">/dev/null 2>&1
cd "${ROOT_PATH}"

APP_VER=`grep SIADRIVE_VERSION_STRING src/siadrive_api/siadrivever.cpp | sed -e "s/\"//g" -e "s/;//g" -e "s/const\ char\ \*SIADRIVE_VERSION_STRING\ \=\ //g"`

if [ "${OSNAME}" == "Linux" ]; then
  ARCHIVE_DIR=SiaDrive_v${APP_VER}_${TARGET_MODE}
  APP_BASE_NAME=${ARCHIVE_DIR}_${OSNAME}_x86_64

  ln -sf ${TARGET_DIR} ${ARCHIVE_DIR} >/dev/null 2>&1 || exit 1
  tar cvzhf ${DIST_DIR}/${APP_BASE_NAME}.tgz ${ARCHIVE_DIR}/ || exit 1
  rm -f ${ARCHIVE_DIR} >/dev/null 2>&1
  cd "${DIST_DIR}"
  shasum -a 512 -b ${APP_BASE_NAME}.tgz > ${APP_BASE_NAME}.sig || exit 1
fi

if [ "${OSNAME}" == "Darwin" ]; then
  ARCHIVE_DIR=SiaDrive.app
  APP_BASE_NAME=SiaDrive_v${APP_VER}_${TARGET_MODE}_${OSNAME}_x86_64

  ln -sf ${DIST_DIR}/${TARGET_MODE}/siadrive.app ${ARCHIVE_DIR} >/dev/null 2>&1 || exit 1
  tar cvzhf ${DIST_DIR}/${APP_BASE_NAME}.tgz ${ARCHIVE_DIR}/ || exit 1
  rm -f ${ARCHIVE_DIR} >/dev/null 2>&1
  cd "${DIST_DIR}"
  shasum -a 512 -b ${APP_BASE_NAME}.tgz > ${APP_BASE_NAME}.sig || exit 1
fi

exit 0