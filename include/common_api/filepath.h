#ifndef _FILEPATH_H
#define _FILEPATH_H
#include <siacommon.h>

NS_BEGIN(Sia)
NS_BEGIN(Api)
class SIADRIVE_EXPORTABLE FilePath {
  public:
    FilePath();
    FilePath(const FilePath &filePath);
    FilePath(const SString &path);
    FilePath(const FilePath &filePath1, const FilePath &filePath2);
    FilePath(const FilePath &filePath1, const SString &path2);
    FilePath(const SString &path1, const FilePath &filePath2);
    FilePath(const SString &path1, const SString &path2);
    FilePath(FilePath &&filePath);

  public:
    ~FilePath();

  public:
    static const SString DirSep;
    static const SString NDirSep;

  public:
    static std::uint64_t CalculateUsedSpace(const FilePath &folder, const bool &recursive);
    static std::int64_t FileSize(const FilePath &filePath);
    static SString FinalizePath(const SString &path);
    static std::vector<FilePath> FindAllFiles(const FilePath &folder, const bool &recursive, const std::int64_t &minSize = 0);
    static std::vector<FilePath> FindAllDirectories(const FilePath &folder, const bool &recursive);
    static SString GetAppDataDirectory();
    static std::uint64_t GetRemainingSpace(const FilePath& folder);
    static SString GetTempDirectory();
    static BOOL RecursiveDeleteFilesByExtension(const FilePath &folder, const SString &extensionWithDot);
    static BOOL RetryDeleteFileIfExists(const FilePath &filePath);

  private:
#ifdef _WIN32
    const ComInitWrapper cw;
#endif
    SString _path;

  public:
    const SString &GetPath() const;
    FilePath &Append(const FilePath &filePath);
    FilePath &Append(const SString &path);
    bool CopyAsFile(const FilePath &filePath) const;
    bool CreateEmptyFile() const;
    bool DeleteAsDirectory() const;
    bool DeleteAsFile() const;
    SString GetExtension(const bool& includeDot) const;
    std::uint64_t GetModifiedTime() const;
    bool IsDirectory() const;
    bool IsFile() const;
    bool IsParentOf(const FilePath &filePath) const;
    bool IsUNC() const;
    FilePath &MakeAbsolute();
    bool MakeDirectory() const;
    bool MoveAsFile(const FilePath &filePath);
    FilePath &RemoveFileName();
    FilePath &Resolve();
    FilePath &StripToFileName();

  public:
    FilePath &operator=(const FilePath &filePath);
    FilePath &operator=(FilePath &&filePath);
    FilePath operator+(const FilePath &filePath) const;
    FilePath operator+(const SString &filePath) const;
    FilePath &operator+=(const FilePath &filePath);
    bool operator==(const FilePath &filePath) const;
    bool operator==(FilePath &&filePath) const;
    bool operator!=(const FilePath &filePath) const;
    bool operator!=(FilePath &&filePath) const;
    SString::SChar &operator[](const size_t &idx);
    const SString::SChar &operator[](const size_t &idx) const;
    operator SString();
    operator SString() const;
};
NS_END(2)
#endif //_FILEPATH_H