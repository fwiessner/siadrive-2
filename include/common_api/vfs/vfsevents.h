#ifndef SIADRIVE_VFSEVENTS_H
#define SIADRIVE_VFSEVENTS_H

#include <siacommon.h>
#include <eventsystem.h>

NS_BEGIN(Sia)
NS_BEGIN(Api)

CEventPtr SIADRIVE_EXPORTABLE CreateVFSErrorEvent(const SString &func, const SString &reason);
CEventPtr SIADRIVE_EXPORTABLE CreateUpgradeProgressEvent(const std::uint64_t& total, const std::uint64_t& remain);
CEventPtr SIADRIVE_EXPORTABLE CreateCurrentUpgradeProgressEvent(const std::uint64_t& total, const std::uint64_t& remain);

NS_END(2)
#endif //SIADRIVE_VFSEVENTS_H
