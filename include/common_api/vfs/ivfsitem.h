#ifndef SIADRIVE_IVIRTUALFILEENTRY_H
#define SIADRIVE_IVIRTUALFILEENTRY_H
#include <siacommon.h>
#include <vfs/vfscommon.h>

NS_BEGIN(Sia)
NS_BEGIN(Api)

template<typename Handle, typename AccessData, typename OpenData>
class IVFSItem {
  INTERFACE_CLASS_COMMON(IVFSItem)

  METHOD(void Close())
  CONST_METHOD(AccessData GetAccessData())
  CONST_METHOD(std::uint64_t GetDate());
  CONST_METHOD(std::int64_t GetSize());
  CONST_METHOD(Handle GetHandle())
  CONST_METHOD(std::uint64_t GetId())
  CONST_METHOD(std::uint64_t GetItemId())
  CONST_METHOD(OpenData GetOpenData())
  CONST_METHOD(SString GetPath())
  CONST_METHOD(SString GetSourcePath())
  CONST_METHOD(VFSItemStatus GetStatus())
  CONST_METHOD(bool IsCached())
  CONST_METHOD(bool IsDirectory())
  METHOD(bool Read(char *data, const std::size_t &dataSize, const std::int64_t &offset, std::size_t &bytesRead))
  METHOD(bool ReadOperation(std::function<bool(Handle handle)> operation, const bool& stubAllowed))
  METHOD(bool Truncate(const std::uint64_t& size));
  METHOD(bool Write(const char *data, const std::size_t &dataSize, const std::int64_t &offset, std::size_t &bytesWritten))
  METHOD(bool WriteOperation(std::function<bool(Handle handle)> operation))
};

template<typename Handle, typename AccessData, typename OpenData>
inline IVFSItem<Handle, AccessData, OpenData>::~IVFSItem() {
}

NS_END(2)
#endif //SIADRIVE_IVIRTUALFILEENTRY_H
