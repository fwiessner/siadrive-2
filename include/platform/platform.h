#ifndef _SIADRIVE_PLATFORM_H
#define _SIADRIVE_PLATFORM_H
#include <siacommon.h>

NS_BEGIN(Sia)
NS_BEGIN(Api)
bool PlatformCheckSingleInstance(const SString &id);
bool PlatformExecuteProcess(std::shared_ptr<CSiaDriveConfig> siaDriveConfig, FilePath process, FilePath workingDir, const bool &waitForExit);
bool PlatformLaunchBundledSiad(std::shared_ptr<CSiaDriveConfig> siaDriveConfig);
bool PlatformIsProcessRunning(const SString &processName);
bool PlatformIsProcessRunningExactPath(const FilePath &filePath);
void PlatformReleaseSingleInstance();
SString PlatformRetrieveString(const SString &key);
SString PlatformSecureDecryptString(const SString &key, const SString &encryptedData);
SString PlatformSecureEncryptString(const SString &key, const SString &data);
void PlatformStoreString(const SString &key, const SString &data);
#ifdef SIADRIVE_PRIVATE_API
std::unique_ptr<ISiaDrive> PlatformCreateSiaDrive(std::shared_ptr<CSiaApi> siaApi, ITransferManager *transferManager);
#endif
NS_END(2)
#endif